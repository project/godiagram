<?php
/**
 * @file
 * Template for displaying Go diagram.
 *
 * Available variables:
 *
 *  - $diagram - Associative array containing:
 *    - $diagram['image'] - Drupal path to image file
 *    - $diagram['title'] - Title string
 */
?>
<div class="godiagram">
    <p><?php print theme('image', array('path' => $diagram['image'], 'attributes' => array('class' => 'diagram_image'))); ?></p>

    <p><span class="diagram_title"><?php print $diagram['title']; ?></span></p>
</div>
