
-- SUMMARY --

The Go Diagram module provides an input filter which will convert textual Go
board position definitions (for the game of Go) to images. Additionally, symbols
can be placed anywhere within text and again the markup for these will be
converted to the appropriate image.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/marshmn/1602068

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1602068


-- REQUIREMENTS --

* Drupal 'Libraries API' module - http://drupal.org/project/libraries

* SL Txt 2 PNG library - Download the latest release from:
  http://senseis.xmp.net/?GoWiki%2FSLTxt2PNG and install into your libraries
  location e.g. sites/all/libraries/sltxt2png

  Note: this module has been tested with the 2009-10-31 release of the library.


-- INSTALLATION --

* Install as usual - for further information, see:
  http://drupal.org/documentation/install/modules-themes/modules-7


-- CONFIGURATION --

* You can enable this filter for any text format by configuring the text format
  in question from Configuration -> Text formats. For the text format in
  question check the box for the filter "Convert Go diagram definitions and
  symbols into images".

  Since this module outputs HTML you should ensure that the order of processing
  of filters is set so that this filter is processed AFTER any filters which may
  strip out HTML tags - e.g. ensure that this filter is placed after the filter
  "Display any HTML as plain text" if enabled.


-- USAGE --

* Once text formats have been configured to enable this input filter, content
  editors will be able to place diagrams and symbols within their content by
  placing the appropriate markup within [godiagram]...[/godiagram] and
  [gosymbol]...[/gosymbol] tags respectively.

  For example, a diagram can be placed with text such as:

  [godiagram]
  $$ A joseki mistake
  $$  ------------------
  $$ | . . . . . . . . .
  $$ | . . 8 . . . . . .
  $$ | . . 4 5 . 2 . 3 .
  $$ | . . 6 1 . . . . .
  $$ | . . 7 a . . . . .
  $$ | . . . . . . . . .
  [/godiagram]

  Symbols can be placed within text such as:

  [gosymbol]BT[/gosymbol]

  For more detail on the syntax for these tags please see the help within Drupal
  which can be accessed from the 'More information about text formats' link when
  editing content.


-- NOTES --

* The current versions of the sltxt2png library make use of PHP's split()
  function which is now deprecated and this will cause warning messages to be
  shown within Drupal. You can turn off the display of these messages from
  Configuration -> Logging and errors.


-- CONTACT --

Current maintainers:
* Matt Marsh (marshmn) - http://drupal.org/user/532744
